.. Introducción al Software Libre documentation master file, created by
   sphinx-quickstart on Sat May  2 18:56:18 2015.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Introducción al Software Libre del litoral 2015
==========================================================

.. header::
  Tecnicatura Universitaria en Software Libre del litoral

Contents:

.. toctree::
   :maxdepth: 2

   introduccion
   licencias
   modelos
   entornos
   casos

.. raw:: pdf

Introduccion
============

La revolución del sofware libre (lo concreto de lo invisible).
No es dificil determinar la importancia del software en nuestra vida cotidiana,
hoy lo utlizamos, sin darnos cuenta, en todo momento: celulares, computadoras,
cajeros automáticos y en todo tipo de situaciones donde se presentan interacciones
con aparatos que brinden tanto información o que sirvan para comunicarnos.

Si nos ponemos a pensar qué es el software, podemos llegar a pensar que es una tecnologia,
y la podemos definir como un conjunto de conocimientos (científicos, sociales,
técnicos, etc) coordinados para actuar sobre la realidad de una determinada manera,
esta forma de actuar esta definida según lo que decide el creador del software o
sea según sus propias conveniencias e intereses, es por ello que decimos que el
software o cualquier tecnología tiene su intencionalidad o mejor dicho las tecnologías
involucran no solo aspectos técnicos, sino tambien una concepción de mundo, de
ética y un planteamiento políitico.
No se pueden escindir lo tecnico de lo etico-político, menos en el software.

En este sentido un sin numero de tecnologias de comunicación e información surgieron
de la construccion y el trabajo de movimientos sociales y políticos que lentamente
el marcado globalizado y sus corporaciones se fueron apropiando.

Los libros que te romperan la cabeza
------------------------------------
- Argentina Copyleft. Fundación Vía Libre.
- MABI. Monopolios Artificiales sobre Bienes Intangibles. Fundación Vía Libre.
- Cultura Libre. Lawrense Lessig.
- Copia este libro. David Bravo.
- Prohibido pensar propiedad privada. Fundación Vía Libre.

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
