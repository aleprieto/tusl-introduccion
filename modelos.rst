.. Introducción al Software Libre documentation master file, created by
   sphinx-quickstart on Sat May  2 18:56:18 2015.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Unidad 3 :: Modelos y procesos de desarrollo de software libre.
==========================================================

Introducción
------------
En 1997 Eric S. Raymond publicó el primer artículo ampliamente difundido, "La catedral y el bazar" (The cathedral and the bazaar. Musings on Linux and open source by an accidental revolutionary, O'Reilly & Associates –http:// www.ora.com–, 2001) [192], en el que se describían algunas características de los modelos de desarrollo de software libre, haciendo especial énfasis en lo que diferenciaba a estos modelos de los de desarrollo propietario. Este artículo se ha convertido desde entonces en uno de los más conocidos (y criticados) del mundo del software libre.

La catedral y el bazar
----------------------
Raymond establece una analogía entre la forma de construir las catedrales medievales y la manera clásica de producir software. Argumenta que en ambos casos existe una clara distribución de tareas y funciones, en la que destaca la existencia de un director  que está por encima de todo y que ha de controlar en todo momento el desarrollo de cada actividad. Asimismo, la planificación está estrictamente controlada, lo que da lugar a unos procesos claramente detallados en los que idealmente cada participante en la actividad tiene un papel específico muy delimitado.

Dentro de lo que Raymond toma como el modelo de creación de catedrales no sólo tienen cabida los procesos pesados que podemos encontrar en la industria del software (el modelo en cascada clásico, las diferentes vertientes del Rational Unified Process, etc.), sino también proyectos de software libre, como es el caso de GNU y NetBSD. Para Raymond, estos proyectos se encuentran fuertemente centralizados, ya que unas pocas personas son las que realizan el diseño y la implementación del software. Las tareas que desempeñan estas personas, así como sus funciones, están perfectamente definidas, y alguien que quisiera entrar a formar parte del equipo de desarrollo necesitaría que se le asignara una tarea y una función según las necesidades del proyecto. Por otra parte, las entregas de este tipo de programas se encuentran espaciadas en el tiempo a partir de una planificación bastante estricta. Esto supone tener pocas entregas del software y ciclos largos, que constan de varias etapas, entre las entregas.

El modelo antagónico al de la catedral, es el bazar. Según Raymond, algunos de los programas de software libre, en especial el núcleo Linux, se han desarrollado siguiendo un esquema similar al de un bazar oriental. En un bazar, no existe una máxima autoridad que controle los procesos que se van desarrollando ni que planifique estrictamente lo que debe hacerse. Además, los roles de los participantes pueden cambiar de manera continua (los vendedores pueden pasar a ser clientes) y sin indicación externa.

Pero lo más novedoso de "La catedral y el bazar" es la descripción del proceso que ha hecho de Linux un éxito dentro del mundo del software libre; es una sucesión de "buenas maneras" para aprovechar al máximo las posibilidades que ofrece la disponibilidad de código fuente y la interactividad mediante el uso de sistemas y herramientas telecomunicaciones y información.

Un proyecto de software libre suele surgir a raíz de una acción puramente personal; la causa se puede encontrar en un desarrollador que vea limitada su capacidad de resolver un problema. El desarrollador debe tener los conocimientos necesarios para, por lo menos, empezar a resolverlo. Una vez que haya conseguido tener algo utilizable, con algo de funcionalidad, sencillo y, a ser posible, bien diseñado o escrito, lo mejor que puede hacer es compartir esa solución con la comunidad del software libre. Es lo que se denomina publicación temprana (release early, en inglés), que permite llamar la atención de otras personas (generalmente desarrolladores) que tengan el mismo problema y que puedan estar interesados en la solución.

Uno de los principios básicos de este modelo de desarrollo es considerar a los usuarios como co-desarrolladores.
Se los ha de tratar bien, porque, además de proporcionar publicidad mediante el "boca a boca", realizarán una de las tareas más costosas que existen en la generación de software: las pruebas. Al contrario que el co-desarrollo, que es difícilmente escalable, la depuración y las pruebas tienen la propiedad de ser altamente paralelizables.
El usuario será el que tome el software y lo pruebe en su máquina bajo unas condiciones específicas (una arquitectura, unas herramientas, etc.), una tarea que multiplicada por un gran número de arquitecturas y entornos supondría un gran esfuerzo para el equipo de desarrollo.

Si se trata a los usuarios como co-desarrolladores puede darse el caso de que alguno de ellos encuentre un error y lo resuelva enviando un parche al desarrollador del proyecto para que el problema esté solucionado en la siguiente versión. También puede suceder, por ejemplo, que una persona diferente de la que descubre un error sea la que finalmente lo entienda y lo corrija. En cualquier caso, todas estas circunstancias son muy provechosas para el desarrollo del software, por lo que es muy beneficioso entrar en una dinámica de esta índole.

Esta situación se torna más efectiva con entregas frecuentes, ya que la motivación para encontrar, notificar y corregir errores es alta debido a que se supone que se va a ser atendido inmediatamente. Además se consiguen ventajas secundarias, como el hecho de que al integrar frecuentemente –idealmente una o más veces al día– no haga falta una última fase de integración de los módulos que componen el programa. Esto se ha denominado publicación frecuente (release often) y posibilita una gran modularidad (Alessandro Narduzzo y Alessandro Rossi, "Modularity
in action: GNU/Linux and free/open source software development model unleashed", mayo 2003), a la vez que maximiza el efecto propagandístico que tiene la publicación de una nueva versión del software.

Para evitar que la publicación frecuente asuste a usuarios que prioricen la estabilidad sobre la rapidez con la que el software evoluciona, algunos proyectos de software libre cuentan con varias ramas de desarrollo en paralelo. El caso más conocido es el del núcleo Linux, que tiene una rama estable –dirigida a aquellas personas que estiman su fiabilidad– y otra inestable –pensada para desarrolladores– con las últimas innovaciones y novedades.

Comunidades de Software Libre
-----------------------------
La comunidad del software libre es un término que hace referencia informal a los usuarios y desarrolladores de software libre, así como los partidarios del movimiento de software libre.
La comunicación en la comunidad del software libre se realiza a través de Internet, listas de correo, wikis y foros, y también mediante conferencias.

"El software que no es libre trae consigo un sistema antisocial que prohíbe la cooperación y la comunidad. Generalmente no se puede ver el código fuente, no se puede saber qué trucos sucios o fallos tontos puede contener. Si a uno no le gusta el programa, no lo puede cambiar. Y lo peor de todo es que está prohibido compartirlo con los demás. Prohibir compartir software es cortar los lazos que unen la sociedad." []
Richard Stallman http://www.gnu.org/philosophy/use-free-software.es.html

Los grupos de usuarios de Linux, también conocidos por su acrónimo GUL (o en inglés LUG, de Linux User Group), suelen ser una asociación local, provincial, regional o nacional, sin ánimo de lucro, que intenta difundir el uso de Linux y del software libre, y su cultura en su ámbito de su actuación, así como representar un punto de apoyo para los propios usuarios, particularmente entre usuarios no experimentados.

En la ciudad de Santa Fe se encuentra el lugli [#]_, un grupo de gente interesada en el sistema operativo GNU/Linux y en la filosofía GNU de software libre, podrás suscribirte a las lista de correo y participar activamente. Seras más que bienvenido.
.. http://lugli.org.ar/index.php/P%C3%A1gina_principal

En Rosario, el Lugro [#]_ tiene como misión la difusión del Software Libre en la región.
.. http://www.lugro.org.ar/

En Argentina, puedes buscar el LUG más cercano a tu localidad en USLA [#]_
.. http://drupal.usla.org.ar/slugs

Si quieres saber más sobre la conformación de los LUGs y su funcionamiento, puedes leer este artículo, traducción al castellano del LUG HOWTO [#]_
.. http://lettherebelight.byethost10.com/docs/COMO-Grupo-de-Usuarios/COMO-Grupo-de-Usuarios.html

Flisol
------
Todos los años, en Latinoamérica y más recientemente en España, se celebra el FLISoL, un evento que está dirigido a todo tipo de público: estudiantes, académicos, empresarios, trabajadores, funcionarios públicos, entusiastas y aún personas que no poseen conocimiento informático.

Este evento se realiza desde el año 2005 y a partir del 2008 se adoptó su realización el 4to Sábado de abril de cada año. La entrada es gratuita y su principal objetivo es promover el uso del software libre, dando a conocer al público en general su filosofía, alcances, avances y desarrollo.

Las comunidades locales de Software Libre son quienes organizan el evento, donde se instala, de manera gratuita y totalmente legal, software libre en las computadoras que llevan los asistentes. Además, en forma paralela, se ofrecen charlas, talleres y otras actividades, sobre temáticas locales, nacionales y latinoamericanas en torno al Software Libre y la Cultura Libre.


Modelos Economicos: como se sustentan
-------------------------------------
El software libre se desarrolla de muchas formas distintas y con mecanismos para conseguir recursos que varían muchísimo de un caso a otro. Cada proyecto libre tiene su propia forma de financiarse, desde el que está formado
completamente por desarrolladores voluntarios y utiliza solamente recursos cedidos de manera altruista, hasta el que es llevado a cabo por una empresa que factura el 100% de sus costes a una entidad interesada en el desarrollo
correspondiente.
En el medio, existe muchas veces financiación externa para proyectos libres que puede considerarse como un tipo de patrocinio, aunque este patrocino no tiene por qué ser desinteresado (y habitualmente no lo es).

Según Richard Stallman, "Hay dos motivaciones comunes para desarrollar un programa libre. Una es que no hay ningún programa que haga esa tarea. Lamentablemente, aceptar el uso de un programa que no es libre elimina esa motivación. La otra es el deseo de ser libre, que motiva a la gente a escribir sustitutos libres de programas privativos. En casos como estos, ese motivo es el único que puede funcionar. Simplemente usando un sustituto libre nuevo y sin terminar, antes de que técnicamente sea comparable con el modelo que no es libre, podemos ayudar a animar a los desarrolladores libres a perseverar hasta que el reemplazo llegue a ser mejor.
Desarrollar sustitutos libres para programas privativos es un gran trabajo, puede llevar años. La tarea probablemente requerirá la ayuda de futuros hackers, gente que es aún muy joven, gente que tendrá que encontrar motivaciones para unirse a nuestro trabajo en el software libre. ¿Qué podemos hacer hoy para ayudar a convencer a otras personas, en el futuro, a mantener la determinación y perseverancia necesarias para terminar esta obra?"
 []
Richard Stallman http://www.gnu.org/philosophy/use-free-software.es.html

Dado que nos referimos a la libertad y no al precio, no existe contradicción alguna entre la venta de
copias y el software libre. De hecho, la libertad para vender copias es crucial: las colecciones de software
libre a la venta en formato de CD­ROM son muy importantes para la comunidad y venderlas es una forma de recaudar   fondos para el desarrollo de software libre. (Stallman, 2004: 28)

Financiación Pública y Privada
..............................
La entidad financiadora puede ser directamente un gobierno (local, regional, nacional o internacional) o una institución pública (por ejemplo, una fundación). En estos casos, la financiación suele ser similar a la de los proyectos de investigación y desarrollo, y de hecho es muy habitual que provenga de entidades públicas promotoras de I+D. Normalmente, la entidad financiadora no busca recuperar la inversión. Muchas veces este tipo de financiamiento no es exclusivo de software libre, en convocatorias orientadas a I+D con un fin social, proyectos desarrollados con Software Libre tiene las de ganar.
En nuestro país, la Agencia Nacional de Promoción Científica y Tecnológica pone a disposición fondos a través de convocatorias y ventanillas permanente, destinados a particulares y empresas para la realización de proyectos de i+d. Particularmente orientadas al desarrollo de software se encuentra: fontar y fonsoft.


Otro tipo de financiación para el desarrollo de software libre, ya no tan altruista, es el que tiene lugar cuando alguien necesita mejoras en un producto libre.
Por ejemplo, para uso interno, una empresa puede necesitar, en un programa dado, cierta funcionalidad o corregir ciertos errores. En estos casos, lo habitual es que la empresa contrate el desarrollo que necesita. Este desarrollo, muy habitualmente (bien porque lo impone la licencia del programa modificado, bien porque la empresa lo decide así), es software libre.
Muchas veces, nos encontramos con personas que necesitan el desarrollo o configuración de un software para resolver un problema, y no conocen la diferencia entre un software privativo y un software libre, ahi es donde nosotros debemos explicarle a la persona las difencias, y mostrarle los motivos por los cuales es preferible un software libre, ya que no sólo tendrá la posibilidad de realizar modificaciones al software adquirido, sino que además, esas modificacioens podrá realizarlas quien desarrolló el software o cualquiera otro desarrollador con conocimientos en el lenguaje de programación específico, esto otorga, a quien posee el software la independencia del proveedor, algo que sin duda, querrá tener.

Modelos de negocio basados en software libre
............................................
En la historia del software, el modelo de negocio clásico está basado en licencias de uso; así trabajan empresas que comercializan licencias de uso de software privativo.
El software libre rompe con ese modelo de negocio, y abre el juego a otras posibilidades. Una de las más conocidas, es, además del desarrollo de un software a medida, la configuración de un software libre existente que soluciona el problema del cliente. Muchas veces, asociado a esta configuración, surge la posiblidad de ampliar funcionalidades, y por lo cual, el negocio pasa por el trabajo de desarrollo de esa funcionalidad en concreto.
Además del desarrollo de software, las empresas que se enfocan exclusivamente en el trabajo con software libre, trabajan fuertemente en la prestación de servicios asociados al desarrollado o implementado.

Distribuciones GNU/Linux
------------------------
Una distribución Linux (coloquialmente llamada distro) es una distribución de software basada en el núcleo Linux que incluye determinados paquetes de software para satisfacer las necesidades de un grupo específico de usuarios, dando así origen a ediciones domésticas, empresariales y para servidores.
Por lo general están compuestas, total o mayoritariamente, de software libre, aunque a menudo incorporan aplicaciones o controladores propietarios.

.. toctree::
   :maxdepth: 2
