.. Introducción al Software Libre documentation master file, created by
   sphinx-quickstart on Sat May  2 18:56:18 2015.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Casos de Estudio :: Software Libre
==================================



Apache
------
Arthur C. Clarke dijo “Cualquier tecnología suficientemente avanzada es indistinguible de la magia”. Internet, sin duda, forma parte del grupo. Pero resulta que no es magia, es tecnología lo que hay detrás. En el caso de las páginas web -que a fin de cuentas es lo que mucha gente considera Internet- lo que hay detrás son servidores que se encargan de proveernos las páginas web que solicitamos con nuestro navegador.

Apache es el servidor web por excelencia, lider abrumador, con un 54% de cuota frente al 24% del IIS de Microsoft- (2010). Probablemente la versatilidad que garantiza el software libre porque cualquiera puede modificar el programa a su antojo es lo que le ha permitido alcanzar tales cuotas de éxito y convertirse, de facto, en el
estandar para servir páginas web.


Mozilla Firefox y Firefox OS
----------------------------
Firefox OS es un sistema operativo móvil, basado en HTML5 con núcleo Linux, de código abierto para varias plataformas. Es desarrollado por Mozilla Corporation bajo el apoyo de otras empresas y una gran comunidad de voluntarios de todo el mundo. El sistema operativo está diseñado para permitir a las aplicaciones HTML5 comunicarse directamente con el hardware del dispositivo usando JavaScript y Open Web APIs.

https://www.mozilla.org/es-AR/

Gnome

Red Hat
-------
Red Hat Inc. es la compañía responsable de la creación y mantenimiento de una distribución del sistema operativo GNU/Linux que lleva el mismo nombre: Red Hat Enterprise Linux, y de otra más, Fedora; en la actualidad también mantiene CentOS. Así mismo, en el mundo del middleware patrocina jboss.org, y distribuye la versión profesional bajo la marca JBoss Enterprise.

Red Hat es famoso en todo el mundo por los diferentes esfuerzos orientados a apoyar el movimiento del software libre, trabajando en el desarrollo de una de las distribuciones más populares de Linux, además de comercializar  diferentes productos y servicios basados en software de código abierto. Asimismo, poseen una amplia infraestructura en la que se cuentan más de 6.000 empleados en 28 lugares del mundo.

El 'sombrero rojo' es una empresa radicada en Carolina del Norte (EEUU). Lleva dedicándose a ofrecer soluciones informáticas basadas en 'software' libre desde 1993, cuando Linux era sinónimo de 'sistema operativo para geeks'. Contra todo pronóstico, esta compañía ha conseguido no solo crecer utilizando este tipo de software, sino consolidarse como una empresa muy estable y con beneficios (casi 100 millones de dólares en 2012).


http://www.eldiario.es/hojaderouter/tecnologia/software/gratis-paradoja-Red_Hat-software_libre_0_276122680.html
