.. Introducción al Software Libre documentation master file, created by
   sphinx-quickstart on Sat May  2 18:56:18 2015.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Unidad 4 :: Entornos y tecnologías de desarrollo de SL
==========================================================

Contents:

.. toctree::
   :maxdepth: 2

...

Espacios de colaboración
------------------------

Herramientas de colaboración
----------------------------

Entorno de desarrollo
---------------------
Control de Versiones
....................

Los sistemas de control de versiones son programas que tienen como objetivo controlar los cambios en el desarrollo de cualquier tipo de software, permitiendo conocer el estado actual de un proyecto, los cambios que se le han realizado a cualquiera de sus piezas.

Sistemas como Git, Subversion, CVS, etc. sirven para controlar las versiones de
un software. Nos ayudan en muchos ámbitos fundamentales, como podrían ser:
* Comparar código de archivos, de modo que podamos ver las diferencias entre versiones
* Restaurar versiones antiguas
* Fusionar cambios entre distintas versiones
* Trabajar con distintas ramas de un proyecto, por ejemplo: producción y desarrollo

En definitiva, con estos sistemas podemos crear y mantener repositorios de software que conservan todos los estados por el que ha pasando la aplicación a lo largo del desarrollo del proyecto. Almacenan también las personas que envian los cambios, las ramas de desarrollo que fueron actualizadas o fusionadas, etc.
El control de versiones, no sólo se utiliza solamente para el desarrollo de software, los mismos conceptos son aplicables a otros ámbitos como documentos, imágenes, sitios web. Los contenidos que estás leyendo en estos momentos, fueron construidos de manera colaborativa a través de herramientas de control de versiones.

Git
,,,
Git es un sistema de control de versiones distribuido. Git nos permite tener repositorio locales  y  remotos, estos repositorios pueden ser accedidos por varios usuarios.

Enlaces de interés
------------------
* http://rogerdudler.github.io/git-guide/index.es.html
* http://www.git-scm.com/book/en/v2
* http://www.git-scm.com/book/es/v1

Github
,,,,,,
Es una una plataforma para alojar proyectos utilizando el sistema de control de versiones Git.
Además, es una comunidad que se ha destacado como un excelente recurso para desarrollar proyectos abiertos.

Enlaces de interés
------------------
* https://guides.github.com/
* https://guides.github.com/activities/hello-world/

Gitlab
,,,,,,
Es una aplicación opensource que nos permite administrar repositorios en git mediante una interfaz web.
Es un proyecto de código libre que se puede instalar en tu propio servidor y que te permite tener repositorios privados, sin costo.

Enlaces de interés
------------------
* https://gitlab.com/help
