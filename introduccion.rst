Unidad 1 :: Introducción al Software Libre
==========================================================

La revolución del software libre (lo concreto de lo invisible)
--------------------------------------------------------------
No es difícil determinar la importancia del software en nuestra vida cotidiana,
hoy lo utilizamos, sin darnos cuenta, en todo momento: celulares, computadoras,
consolas de juego, cajeros automáticos y en todo tipo de situaciones donde se
presentan interacciones con aparatos que brinden tanto información o que sirvan
para comunicarnos.

Si nos ponemos a pensar que es el software decimos que es una tecnología, y
podemos definirla como un conjunto de conocimientos (científicos, sociales, y
técnicos) coordinados para actuar sobre la realidad de una determinada manera.
Esta forma de actuar, en el caso del software, está definida según lo que decide
el o lxs creadores del software o sea según sus propias conveniencias e intereses,
es por ello que decimos que el software o cualquier tecnología tiene su
intencionalidad o mejor dicho las tecnologías involucran no solo aspectos técnicos,
sino también concepciones del mundo, de ética y por ello un planteamiento político.

No es posible entonces, y carece de sentido, escindir lo técnico de lo ético-político,
menos en el software por su carácter de producción netamente social (y en condiciones
artesanales, muy lejos de ser industrial).

Muchos proyectos de tecnologías de comunicación e información (entre ellos de software),
han surgido y son sostenidos por el trabajo sinérgico de movimientos socio-técnicos
y políticos que luchan por una sociedad mas justa, generosa, y por los ideales de
la colaboración y el compartir libremente sin ataduras de ninguna índole.

Esta construcción no ha sido, ni está exenta de tensiones y contrapuntos con la
maquinaría infernal de las grandes corporaciones de software y sus modelos de negocio,
ligados al copyright y al patentamiento. Entre estas resistencias y tensiones también
se ha construido el derrotero del software libre.

Esta cátedra de Introducción al Software Libre (SL) intentará reunir los principales
hitos de su historia y contexto de surgimiento como tecnología y de su comunidad
sociotecnopolítica, cómo así también de los aspectos legales y técnicos que han
construido al software libre cómo una tecnología real, viable, y sostenible.



¿Qué es el Software Libre?
--------------------------
Si tuvieramos que resumirlo en una sola frase, diríamos que el «Software libre»
es el software que respeta la libertad de los usuarios.

Según la Free Software Fundation:

"el «Software libre» es el software que respeta la libertad de los usuarios y la
comunidad. A grandes rasgos, significa que los usuarios tienen la libertad de ejecutar,
copiar, distribuir, estudiar, modificar y mejorar el software. Es decir, el
«software libre» es una cuestión de libertad, no de precio.
Para entender el concepto, piense en «libre» como en «libre expresión», no como
en «barra libre». En inglés a veces decimos «libre software», en lugar de
«free software», para mostrar que no queremos decir que es gratuito." [#]_

.. [#] http://www.gnu.org/philosophy/free-sw.es.html

independientemente de sus características técnicas, el software libre presenta ventajas
sociales, políticas y económicas.

Algunas reflexiones.

El puntapié inicial del movimiento de software libre fue el lanzamiento del proyecto
GNU en 1984, cuyo objetivo es la producción de un entorno de software completamente
libre, con la idea de mostrar que el software se puede producir y compartir de otra
manera. [#]_

.. [#] Federico Heinz, "Argentina Copyleft", Software Libre: La revolución constructiva

El software libre nos devuelve el poder de aprender y de hacer nosotros mismos lo
que deseemos en el campo informático. Esto es aplicable a casos de personas como
individuos o a sociedades enteras como colectivos que priorizan el bien social por
encima de los intereses de una empresa en particular.
Adaptar nuestro sistema educativo a las condiciones fijadas por una empresa suena
irrisorio, sin embargo, es lo que estamos haciendo, a la vez que hipotecamos el
futuro, porque nuestros jóvenes no tiene acceso real a lo que es la técnica cultural
de nuestro tiempo, la informática, la programación.
Nada de eso es para nosotros en este modelo. Para nosotros, sólo el consumo. Para
nosotros, sólo comprar hecho. Para nosotros, dependencia. Y si lo compartimos,
penalización.  [#]_

..[#] Beatriz Busaniche, "Libres de monopolios sobre la vida y el conocimiento.
Hacia una convergencia de movimientos". Cap. Software Libre para sociedades libres, p 83.


Las 4 Libertades
----------------
Si alguna vez fuimos a una charla sobre software libre, seguramente hemos escuchado
hablar de las libertades. Detallamos las mismas a continuación:

0. La libertad usar el software para cualquier propósito (libertad 0).
1. La libertad de estudiar cómo funciona el programa, y modificarlo, adaptándolo a las necesidades propias (libertad 1). El acceso al código fuente es una condición necesaria para ello.
2. La libertad de distribuir copias para ayudar a otros usuarios (libertad 2).
3. La libertad de mejorar el software y hacer públicas esas mejoras, de modo que toda la comunidad se beneficie (libertad 3). El acceso al código fuente es una condición necesaria para ello.

El mecanismo que se utiliza para garantizar estas libertades, de acuerdo con la
legalidad vigente, es la distribución mediante una licencia determinada.
En la licencia, el autor plasma su permiso para que el usuario del software pueda
ejercer esas libertades, y también las restricciones que pueda querer aplicar.
Para que a licencia sea considerada libre, debe respetar las 4 libertades mensionadas.

GNU/Linux
------------------
El conjunto de GNU/Linux es un sistema operativo compatible con Unix. Linux es el núcleo del sistema operativo
(kernel) el corazón del sistema operativo.
El proyecto GNU (GNU no es Unix), viene de las herramientas básicas del sistema operativo creadas por el proyecto
GNU, iniciado por Richard Stallman en 1983 tiene como objetivo el desarrollo de un sistema operativo Unix completo
y compuesto enteramente de Software Libre.

La historia del núcleo Linux esta fuertemente vinculada al proyecto GNU. En 1991 Linus Torvalds empezó a trabajar
en un reemplazo no comercial para MINIX que más adelante acabaría siendo Linux.
Cuando Linus Torvalds libero la primer versión de Linux, el proyecto GNU ya había producido varias de las
herramientas fundamentales para el manejo del sistema operativo, incluyendo un intérprete de comandos, una
biblioteca C y un compilador, pero el proyecto GNU contaba ya con una infraestructura para crear su propio
sistema operativo el llamado Hurd y este aún no era lo suficiente maduro para usarse, comenzaron a usar a Linux
a modo de continuar desarrollando el proyecto GNU, siguiendo la tradicional filosofía de mantener cooperativita
entre desarrolladores. El día en que se estime que Hurd es suficiente maduro y estable, será llamado a reemplazar
a Linux.
Entonces, el núcleo creado por Linus Torvalds, quien se encontraba por entonces estudiando en la Universidad
de Helsinki, lleno el espacio final que había en el sistema operativo de GNU.

Minix es una réplica gratuita del sistema operativo Unix distribuido junto con su código fuente, con propósitos
educativos, que luego inspiro a Linux desarrollado por el profesor Andrew S. Tanenbaum en 1987.
La ultima versión oficial de Minix es la 3.1.8, publicada el 4 de Octubre del 2010 que se encuentra en la
siguiente url : http://www.minix3.org.

La historia completa sobre el proyecto GNU/Linux se puede leer el el libro "Software Libre para una sociedad libre"
, en el capítulo I: El proyecto GNU y el Software Libre, de Richard Stallman.

Diferencia con otros tipos de software
--------------------------------------

- De código abierto

  El software de código abierto es un software que pone a disposición de cualquier
  usuario su código fuente, y otorga derechos para utilizar, modificar y distribuir
  el software.

  La Open Source Definition introduce una filosofía en cuanto al código abierto, y
  además define los términos de uso, modificación y redistribución del software de
  código abierto. [#]_
  ..[#] http://opensource.org/osd

  Según Stallman, "El código abierto es una metodología de programación, el software
  libre es un movimiento social. Para el movimiento del software libre, el software
  libre es un imperativo ético, respeto esencial por la libertad de los usuarios.
  En cambio la filosofía del código abierto plantea las cuestiones en términos de
  cómo «mejorar» el software, en sentido meramente práctico" [#]_

  .. [#] http://www.gnu.org/philosophy/open-source-misses-the-point.html


- Privativo

  El software privativo se encuentra amparado bajo licencias que reservan algunos
  o todos los derechos de uso, copia, modificación y distribución para el fabricante,
  quien previo pago de una regalía, concede el uso de una copia ejecutable del programa
  al titular de la licencia, el usuario.
  En este caso, el usuario no es dueño del software que está funcionando en su computadora,
  el propietario sigue siendo el fabricante y no otorga al usuario la facultad de
  realizar modificación alguna al software.
  El software privativo, al no permitir conocer el código fuente, no permitir estudiar
  la forma en que el sistema realiza sus funciones, esto hace que el usuario no pueda
  saber si el software que está usando realiza cosas que no desea.

- Freeware o Gratis

  Un software que es gratis no nos dice nada respecto de las libertades del usuario,
  respecto de las posibilidades de estudiar, compartir y distribuir el mismo.
  Cualquier software que no exija un monto por su adquisición es Freeware, sea software
  libre, privativo o de código abierto. Esta aclaración solo se hace para evitar
  cualuqier confusión respecto al software libre o de código abierto.
  Debemos tener en claro que el software libre puede o no ser gratis.
  Es habitual que este tipo de sistemas se utilicen para promocionar otros programas
  (típicamente con funcionalidad más completa) o servicios.

- Shareware

  No es siquiera software gratis, es un método de distribución, ya que los programas,
  generalmente sin códigos fuente, se pueden copiar libremente, pero no usar continuadamente
  sin pagarlos. En esta modalidad el usuario puede evaluar de forma gratuita el producto,
  pero con limitaciones en el tiempo de uso o en algunas de las formas de uso.
  La exigencia de pago puede estar incentivada por funcionalidad limitada.

En un video publicado en la web, nos explican de una manera muy divertida la diferencia
entre el software libre y el software propietario, haciendo una analogía entre la
receta de un apetitoso pastel y el acceso al código fuente de un software.
https://vimeo.com/28316416


Hardware Libre
--------------
Podemos denominar hardware libre, a todos aquellos dispositivos de hardware cuyas
especificaciones y diagramas esquemáticos sean de acceso público. Al querer aplicar
las libertades del software libre al hardware, nos encontramos con algunas dificultades
propias de la distinta naturaleza entre ambos.

No se pueden aplicar directamente las cuatro libertades del software libre al hardware,
dada su diferente naturaleza. Uno tiene existencia física, el otro no. Aparecen
una serie de problemas:

Un diseño físico es único. Si yo construyo una placa, es única. Para que otra persona
la pueda usar, bien le dejo la mía o bien se tiene que construir una igual. La compartición
tal cual la conocemos en el mundo del software NO ES POSIBLE
La compartición tiene asociado un coste. La persona que quiera utilizar el hardware
que yo he diseñado, primero lo tiene que fabricar, para lo cual tendrá que comprobar
los componentes necesarios, construir el diseño y verificar que se ha hecho correctamente.
Todos esto tiene un coste.
Disponibilidad de los componentes. ¿Están disponibles los chips?. Al intentar fabricar
un diseño nos podemos encontrar con el problema de la falta de material. En un país
puede no haber problema, pero en otro puede que no se encuentran.

Una primera propuesta para definir el hardware libre es la siguiente
El hardware libre ofrece las mismas cuatro libertades que el software libre, pero
aplicadas a los planos del hardware.
Si en el software hablamos de fuentes, aquí hablamos de planos. A partir de ellos
podemos fabricar el hardware. El proceso de construcción tiene asociado un coste,
que no existe en el caso del software. Sin embargo los planos están disponibles
para que cualquiera los pueda usar, modificar y distribuir. [#]_

  .. [#] http://www.iearobotics.com/personal/juan/publicaciones/art4/html/hispalinux-articulo.html

Tal como detallamos previamente en este capítulo, respecto de las discusiones entorno
al software libre y el software de código abierto; en el mundo del hardware libre
florecen las mismas discusiones, por eso a continuación, una definción que nos brinda
la Open Source Hardware Association.

Hardware de Fuentes Abiertas (OSHW en inglés) es aquel hardware cuyo diseño se hace
disponible públicamente para que cualquier persona lo pueda estudiar, modificar,
distribuir, materializar y vender, tanto el original como otros objetos basados
en ese diseño. [#]_

  .. [#] http://www.oshwa.org/definition/spanish/

Algunos proyectos interesantes con Hardware Libre
-------------------------------------------------

- Arduino [#]_
  .. [#] http://www.arduino.cc/

- Raspberry Pi [#]_
  .. [#] https://www.raspberrypi.org/

- RepRap [#]_
  .. [#] http://reprap.org/wiki/RepRap_en_espa%C3%B1ol_-_RepRap_in_Spanish


Cultura Libre
--------------
Todas las libertades que hemos estudiado asociada al software, se pueden aplicar
a la cultura.

Tal como bien reflexiona Lawrence Lessig, una cultura libre no es una cultura sin
propiedad; no es una cultura en la que no se paga a los artistas. Una cultura sin
propiedad, o en la que no se paga a los artistas, es la anarquía, no la libertad.

Construir cultura, compartir conocimiento, poner en común lo que sabemos, lo que
podemos dar y lo que sin dudas, con el correr del tiempo, definitivamente nos hará
libres.

Hace algunos años, Eben Moglen, uno de los principales referentes de la comunidad
de Software Libre, planteaba que la lucha por la libertad de expresión en nuestro
tiempo se estructura en cuatro aspectos fundamentales: el hardware libre, el software
libre, la cultura libre y el espectro radioeléctrico libre. Estos cuatro aspectos
que hacen a la comunicación en la era digital están de algún modo cubiertos en las
propuestas de esta convergencia por la cultura libre que tratamos de construir.
Se trata, ni más ni menos, que de la democratización de las herramientas de nuestro
tiempo, la libertad y la participación en la cultura y el ejercicio pleno del derecho
a la comunicación y a la educación. [#]_
.. [#] Argentina Copyleft, Introducción.


Estamos preparados para la revolución? Música, Libros
Si el derecho de autor nacido para proteger la cultura se ha convertido en el principal
arma para asfixiarla es que algo está fallando. Si esto es así, es que se han olvidado
los fines a los que sirven esos derechos. Si esto es así, es que hay que cambiarlo
todo.

Existen muchos casos interesantes para reflexionar sobre las posibilidades que brindan
los avances tecnológicos actuales y los modelos de negocios existentes que ven amenazado
su normal funcionamiento por dichos avances.
Internet puso a disposición de todos los usuarios la posibilidad de compartir recursos
sin costo adicional. a

- Caso Taringa [#]_
  .. [#] http://derechoaleer.org/blog/2012/05/taringa-y-el-delito-que-nos-afec.html

- Caso The Pirate Bay [#]_
  .. [#] http://derechoaleer.org/blog/2014/07/the-pirate-bay-el-catalogo-de-la-biblioteca-de-babel.html

- Cadra-UBA [#]_
  .. [#] http://derechoaleer.org/blog/2013/06/infografia-la-estafa-cadra-uba.html

- Caso Cuevana [#]_
  .. [#] http://derechoaleer.org/blog/2012/03/expediente-cuevana.html

Alternativas libres:

- guifi.net [#]_
  .. [#] http://guifi.net

- Red social libre: Lorea.org [#]_
  .. [#] http://lorea.org

- Oiga.me [#]_
  .. [#] http://oiga.me
