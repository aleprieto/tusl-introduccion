.. Introducción al Software Libre documentation master file, created by
   sphinx-quickstart on Sat May  2 18:56:18 2015.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Definición de la libertad en el software.
==========================================================

* Introducción
* Las 4 Libertades del Software Libre
* Software libre y cultura libre
* Diferencias entre Software Libre y Open source
* Surgimiento y contexto
* Raíces y actualidad del movimiento político técnico
* Software Libre en el Estado
